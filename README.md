# README #

The IntelliJ Plugin belonging to `config-reader`.

### How do I get set up? ###

Run the plugin
```
./gradlew clean runIde
```

### Contribution guidelines ###

* Open an issue and provide a pull request resolving the issue
* Writing tests
* Code review
